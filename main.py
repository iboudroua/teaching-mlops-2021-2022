import bottle
import spacy
import logging
from json import dumps

logging.info("Loading model..")
nlp = spacy.load("./models")

@bottle.route("/api/intent")
def intent_inference() :
    sentence = bottle.request.query['sentence']
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    bottle.response.content_type = "application/json"
    return dumps(inference.cats)

@bottle.route("/api/intent-supported-languages")
def supported_languages():
    bottle.response.content_type = "application/json"
    return dumps(["fr-FR"])

@bottle.route("/health")
def health_check_endpoint():
    return bottle.HTTPResponse(status=200)

bottle.run(bottle.app(), host='0.0.0.0', port=8080, debug=True, reloader=True)